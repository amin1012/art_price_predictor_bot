"""Settings."""
import os
from pathlib import Path

TELEGRAM_TOKEN: str = os.getenv("TELEGRAM_TOKEN", "")
URL: str = os.getenv("ART_PRICE_PREDICTOR_URL", "")

IMAGGA_API_KEY = os.getenv("IMAGGA_API_KEY", "")
IMAGGA_SECRET = os.getenv("IMAGGA_API_SECRET", "")
URL_IMAGGA_TAGS: str = "https://api.imagga.com/v2/tags"


JOURNEY_URL = "https://api-inference.huggingface.co/models/FredZhang7/paint-journey-v1"
JOURNEY_TOKEN = os.getenv("JOURNEY_TOKEN", "")

START, PRICE_PREDICTOR_BY_UPLOAD_IMAGE, GENERATE_IMAGE, WOLF_MEMES = range(4)

COLLECT_IMAGGA = True

GENERATED_IMAGE_FILE_NAME: Path = (
    Path().cwd() / "art_price_predictor_bot/data/generate/generated_image.jpg"
)

WOLF_MEMES_LIST = os.listdir(Path().cwd() / "art_price_predictor_bot/data/wolf_memes")
