"""Image structure."""

from pydantic import BaseModel


class ImageRequest(BaseModel):
    """Class ImageRequest for request."""

    file: bytes
    filename: str

    class Config:
        """Config."""

        json_encoders = {bytes: lambda bs: "".join(map(chr, bs))}
