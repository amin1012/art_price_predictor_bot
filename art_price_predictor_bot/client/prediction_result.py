"""Prediction result."""

from pydantic import BaseModel


class PredictionResult(BaseModel):
    """Prediction result."""

    name: str
    price: int
    currency: str
