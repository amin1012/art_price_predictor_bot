"""Http client."""
import asyncio
import typing

from art_price_predictor_bot.client.image import ImageRequest
from art_price_predictor_bot.client.request import (
    collect_generate_image,
    collect_prediction_result_from_art_price_predictor,
    collect_prediction_result_from_imagga,
)
from art_price_predictor_bot.settings import (
    IMAGGA_API_KEY,
    IMAGGA_SECRET,
    JOURNEY_TOKEN,
    JOURNEY_URL,
    URL,
    URL_IMAGGA_TAGS,
)


class Client:
    """Class Client to connect with api."""

    def __init__(
        self,
        url: str,
        imagga_api_key: str,
        imagga_secret: str,
        imagga_url: str,
        journey_url: str,
        journey_token: str,
    ) -> None:
        """
        Args:
            url: url
            imagga_url: imagga url
            imagga_secret: imagga secret
            imagga_api_key: imagga api key
            journey_url: journey url,
            journey_token: journey token
        Returns:
            None
        """
        self.url = url
        self.imagga_api_key = imagga_api_key
        self.imagga_secret = imagga_secret
        self.imagga_url = imagga_url
        self.journey_url = journey_url
        self.journey_token = journey_token

    async def predict(
        self, image_bytes: bytes, filename, collect_imagga: bool = True
    ) -> dict[str, typing.Union[str, int, list]]:
        """Main method to predict output for give image.

        Args:
            image_path: image
            collect_imagga: flag to collect imagga or not
            filename: file name
        Returns:
            predicted attributes.
        """

        image_request: ImageRequest = ImageRequest(
            file=image_bytes,
            filename=filename,
        )

        task_art_predictor = asyncio.create_task(
            collect_prediction_result_from_art_price_predictor(
                url=self.url,
                request=image_request,
            )
        )
        if collect_imagga:
            task_imagga = asyncio.create_task(
                collect_prediction_result_from_imagga(
                    url=self.imagga_url,
                    image_bytes=image_bytes,
                    api_key=self.imagga_api_key,
                    api_secret=self.imagga_secret,
                )
            )

            prediction_results = await asyncio.gather(task_art_predictor, task_imagga)
        else:
            prediction_results = await asyncio.gather(task_art_predictor)

        prediction_result = {}
        for result in prediction_results:
            prediction_result.update(result)

        return prediction_result

    def generate_image(self, text: str) -> bytes:
        """Generates image.

        Args:
            text: text
        Returns:
            generated image
        """

        return collect_generate_image(
            url=self.journey_url, token=self.journey_token, text=text
        )


client = Client(
    url=URL,
    imagga_url=URL_IMAGGA_TAGS,
    imagga_secret=IMAGGA_SECRET,
    imagga_api_key=IMAGGA_API_KEY,
    journey_url=JOURNEY_URL,
    journey_token=JOURNEY_TOKEN,
)
