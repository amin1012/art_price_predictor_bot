"""Requests on fastapi."""

import typing

import requests
from art_price_predictor_bot.client.image import ImageRequest
from art_price_predictor_bot.client.prediction_result import PredictionResult
from loguru import logger


async def collect_prediction_result_from_art_price_predictor(
    url: str,
    request: ImageRequest,
) -> dict[str, typing.Union[str, int, float]]:
    """Collecting prediction result from art price predictor.

    Args:
        url: url
        request: request
    Returns:
        predicted result
    """
    logger.info("Request to fastapi")
    try:
        response: requests.Response = requests.request(
            "POST",
            f"{url}/predict",
            files={"file": request.file},
            params={"filename": request.filename},
            timeout=10,
        )
    except requests.exceptions.ConnectionError:
        return {}
    except requests.exceptions.Timeout:
        return {}

    logger.info(f"STATUS CODE: {response.status_code}")
    if response.status_code != 200:
        return {}

    prediction_result: PredictionResult = PredictionResult.parse_raw(response.content)

    return prediction_result.dict()


async def collect_prediction_result_from_imagga(
    url: str,
    image_bytes: bytes,
    api_key: str,
    api_secret: str,
) -> dict[str, list[typing.Union[float, int, str]]]:
    """Collecting prediction result from Imagga api.

    Args:
        url: url
        image_bytes: image path
        api_key: api key
        api_secret: api secret
    Returns:
        prediction result
    """
    logger.info("Request to imagga api")
    try:
        response = requests.post(
            url,
            auth=(api_key, api_secret),
            files={"image": image_bytes},
            timeout=30,
        )
    except requests.exceptions.ConnectionError:
        return {}
    except requests.exceptions.Timeout:
        return {}
    logger.info(f"STATUS CODE: {response.status_code}")
    if response.status_code != 200:
        return {}

    output = response.json()
    prediction_result_dict: dict[str, list[typing.Union[float, int, str]]] = {}
    tags: list[typing.Union[float, int, str]] = []
    confidences: list[typing.Union[float, int, str]] = []
    for out in output["result"]["tags"][:5]:
        tags.append(out["tag"]["en"])
        confidences.append(out["confidence"])

    prediction_result_dict["confidences"] = confidences
    prediction_result_dict["tags"] = tags

    return prediction_result_dict


def collect_generate_image(url: str, token: str, text: str) -> bytes:
    """Collect image from journey.

    Args:
        url: url
        token: token
        text: text
    Returns:
        generated image
    """

    headers = {"Authorization": f"Bearer {token}"}
    payload = {"inputs": text}
    logger.info("Request to journey")
    try:
        response = requests.post(url, headers=headers, json=payload, timeout=30)
    except requests.exceptions.ConnectionError:
        return b""
    except requests.exceptions.Timeout:
        return b""

    logger.info(f"STATUS CODE: {response.status_code}")
    if response.status_code != 200:
        return b""

    return response.content
