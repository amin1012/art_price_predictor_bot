"""Formatting output."""


import typing


def format_tags_and_confidences(res: dict[str, typing.Union[str, int, list]]) -> str:
    """Formats predictions to output format.

        Args:
            res: prediction result
        Returns:
            Data in format: tag1, conf1\n, tag2, conf2\n, ....
    """

    if "tags" not in res.keys():
        return ""
    tags = res["tags"]
    confs = res["confidences"]
    if isinstance(tags, list) and isinstance(confs, list):
        n = len(tags)
    else:
        return ""
    output: str = ""
    for i in range(n):
        output += f"tag: {tags[i]}, confidence: {round(confs[i], 2)}\n"

    return output


def format_price(res: dict[str, typing.Union[str, int, list]]) -> typing.Union[int, None]:
    """Formats prediction for price.

        Args:
            res: prediction result
        Returns:
            price
    """

    if "price" not in res.keys() or not isinstance(res["price"], int):
        return None

    return res["price"]
