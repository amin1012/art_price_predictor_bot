"""Test processing."""
import typing
from pathlib import Path

import requests
from art_price_predictor_bot.client.client import Client
from art_price_predictor_bot.client.prediction_result import PredictionResult
from art_price_predictor_bot.settings import (
    IMAGGA_API_KEY,
    IMAGGA_SECRET,
    JOURNEY_TOKEN,
    JOURNEY_URL,
    URL,
    URL_IMAGGA_TAGS,
)

client = Client(
    url=URL,
    imagga_url=URL_IMAGGA_TAGS,
    imagga_secret=IMAGGA_SECRET,
    imagga_api_key=IMAGGA_API_KEY,
    journey_url=JOURNEY_URL,
    journey_token=JOURNEY_TOKEN,
)


def load_test_image_1() -> Path:
    """Gets test image 1."""
    return Path().cwd() / "art_price_predictor_bot/data/test_image_1.jpeg"


def load_test_image_2() -> Path:
    """Gets test image 2."""
    return Path().cwd() / "art_price_predictor_bot/data/test_image_2.jpg"


def load_test_image_3() -> Path:
    """Gets test image 3."""
    return Path().cwd() / "art_price_predictor_bot/data/test_image_3.jpg"


def entities_test_image_1() -> dict[str, typing.Union[str, int, list]]:
    """Entities for test image 1."""

    return {
        "price": 2580,
    }


def entities_test_image_2() -> dict[str, typing.Union[str, int, list]]:
    """Entities for test image 2."""

    return {
        "price": 3317,
    }


def entities_test_image_3() -> dict[str, typing.Union[str, int, list]]:
    """Entities for test image 3."""

    return {
        "price": 2386,
    }


def test_image_1() -> None:
    """Test image 1."""

    image_path = load_test_image_1()
    expected_entities = entities_test_image_1()

    response = requests.request(
        "POST",
        f"{URL}/predict",
        files={"file": image_path.read_bytes()},
        params={"filename": str(image_path)[-5:]},
        timeout=10,
    )
    prediction_result = PredictionResult.parse_raw(response.content)
    res = prediction_result.dict()
    for key, value in expected_entities.items():
        pred_value = res[key]
        assert pred_value == value


def test_image_2() -> None:
    """Test image 2."""

    image_path = load_test_image_2()
    expected_entities = entities_test_image_2()

    response = requests.request(
        "POST",
        f"{URL}/predict",
        files={"file": image_path.read_bytes()},
        params={"filename": str(image_path)[-5:]},
        timeout=10,
    )
    prediction_result = PredictionResult.parse_raw(response.content)
    res = prediction_result.dict()
    for key, value in expected_entities.items():
        pred_value = res[key]
        assert pred_value == value


def test_image_3() -> None:
    """Test image 3."""

    image_path = load_test_image_3()
    expected_entities = entities_test_image_3()

    response = requests.request(
        "POST",
        f"{URL}/predict",
        files={"file": image_path.read_bytes()},
        params={"filename": str(image_path)[-5:]},
        timeout=10,
    )
    prediction_result = PredictionResult.parse_raw(response.content)
    res = prediction_result.dict()
    for key, value in expected_entities.items():
        pred_value = res[key]
        assert pred_value == value
