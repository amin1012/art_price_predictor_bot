"""Messages."""

from string import Template

HELP: str = "Type /start to get start"

STOPPED_BOT: str = "Bot stopped. /start to run bot again."

DEFAULT_MESSAGE: str = "Sorry, I don't understand that. Run /start to get start."

UPLOAD_PHOTO_PLEASE: str = "Upload photo please."

UPLOAD_FILE: str = "Upload file"

DONT_GET_PHOTO: str = "I don't eat photo. Type /start to start."

WRONG_TEXT: str = "Sorry, server is full now. Generating unavailable. Try later."

STOP_GENERATE: str = "Type /stop to stop generate."

ESTIMATED_PRICE: Template = Template(
    """Estimated price: $price $$
Possible tags and confidences:\n $tags_and_confidences\n
Wolf quote:  $wolf_quote"""
)

STOP_OR_GENERATE_NEW: str = (
    "Type /stop to stop. \n Or push continue button to create new image."
)


TYPE_GENERATE_PROMPT: str = "Type what you want to generate"

CONTINUE_UPLOAD_IMAGE: str = (
    "You can to continue to upload image. Or Type /stop to stop."
)

WATCH_MEMES: str = "Type any symbol to start watching memes."

STOP_WATCH_MEMES: str = "Type /stop to stop."
