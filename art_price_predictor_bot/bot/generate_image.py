"""Generate Image."""
from pathlib import Path

from art_price_predictor_bot.bot.messages import (
    ESTIMATED_PRICE,
    STOP_GENERATE,
    STOP_OR_GENERATE_NEW,
    TYPE_GENERATE_PROMPT,
    WRONG_TEXT,
)
from art_price_predictor_bot.client.client import client
from art_price_predictor_bot.emodzi import MONEY, ROCKET
from art_price_predictor_bot.formating.format import (
    format_price,
    format_tags_and_confidences,
)
from art_price_predictor_bot.formating.wolf_quote_generator import (
    generate_random_wolf_quote,
)
from art_price_predictor_bot.settings import (
    COLLECT_IMAGGA,
    GENERATE_IMAGE,
    GENERATED_IMAGE_FILE_NAME,
)
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import ContextTypes


async def generate_image(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Generate image."""
    if update.message is not None:
        keyboard = [
            [
                InlineKeyboardButton(
                    "Estimate image " + MONEY, callback_data="estimate_generated_image"
                ),
            ],
            [
                InlineKeyboardButton(
                    "Continue" + ROCKET, callback_data="continue_generate"
                ),
            ],
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)

        text = update.message.text
        generated_image: bytes = client.generate_image(text)
        if not generated_image:
            await update.message.reply_text(WRONG_TEXT)
            await update.message.reply_text(STOP_GENERATE, reply_markup=reply_markup)
            return GENERATE_IMAGE
        await update.message.reply_photo(generated_image)
        generated_image_code: Path = GENERATED_IMAGE_FILE_NAME
        with open(generated_image_code, "wb") as f:
            f.write(generated_image)

        await update.message.reply_text(STOP_GENERATE, reply_markup=reply_markup)

    return GENERATE_IMAGE


async def estimate_generated_image(
    update: Update, context: ContextTypes.DEFAULT_TYPE
) -> int:
    """Estimates generated image."""

    query = update.callback_query

    photo_bytes_conv: bytes = GENERATED_IMAGE_FILE_NAME.read_bytes()

    res = await client.predict(
        photo_bytes_conv,
        filename=str(GENERATED_IMAGE_FILE_NAME),
        collect_imagga=COLLECT_IMAGGA,
    )

    tags_and_confidences: str = format_tags_and_confidences(res=res)
    price = format_price(res=res)
    wolf_quote = generate_random_wolf_quote()
    caption: str = ESTIMATED_PRICE.substitute(
        price=price,
        tags_and_confidences=tags_and_confidences,
        wolf_quote=wolf_quote,
    )
    # Price is estimated using Sothebys.com data
    # Other characteristics can be predicted using Wikiart data
    if query is not None and query.message is not None:
        await query.message.reply_photo(
            photo=photo_bytes_conv,
            caption=caption,
        )

        keyboard = [
            [
                InlineKeyboardButton(
                    "Estimate image" + MONEY, callback_data="estimate_generated_image"
                ),
            ],
            [
                InlineKeyboardButton("Generate new", callback_data="continue_generate"),
            ],
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        await query.message.reply_text(STOP_OR_GENERATE_NEW, reply_markup=reply_markup)

    return GENERATE_IMAGE


async def type_text_to_generate(
    update: Update, context: ContextTypes.DEFAULT_TYPE
) -> int:
    """Type text to generate image."""

    query = update.callback_query
    if query is not None:
        await query.edit_message_text(TYPE_GENERATE_PROMPT)

    return GENERATE_IMAGE


async def continue_generate(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Continue generate."""

    query = update.callback_query

    if query is not None:
        await query.edit_message_text(TYPE_GENERATE_PROMPT)

    return GENERATE_IMAGE
