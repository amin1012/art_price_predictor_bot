"""Price predict for upload image."""
from art_price_predictor_bot.bot.messages import (
    CONTINUE_UPLOAD_IMAGE,
    ESTIMATED_PRICE,
    UPLOAD_FILE,
)
from art_price_predictor_bot.client.client import client
from art_price_predictor_bot.formating.format import (
    format_price,
    format_tags_and_confidences,
)
from art_price_predictor_bot.formating.wolf_quote_generator import (
    generate_random_wolf_quote,
)
from art_price_predictor_bot.settings import (
    COLLECT_IMAGGA,
    PRICE_PREDICTOR_BY_UPLOAD_IMAGE,
)
from loguru import logger
from telegram import Update
from telegram.ext import ContextTypes


async def estimate_price(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Estimate a price for photo."""
    # TODO: можно заслать много фото.
    if update.message is not None and update.effective_user is not None:
        logger.info(update.effective_user.username)
        photo_file = await update.message.photo[-1].get_file()
        logger.info(f"{photo_file.file_path}")
        photo_bytes: bytearray = await photo_file.download_as_bytearray()
        photo_bytes_conv: bytes = bytes(photo_bytes)
        if photo_file is not None and photo_file.file_path is not None:
            filename = photo_file.file_path[-5:]
        logger.info(filename)

    res = await client.predict(
        photo_bytes_conv, filename=filename, collect_imagga=COLLECT_IMAGGA
    )

    tags_and_confidences: str = format_tags_and_confidences(res=res)
    price = format_price(res=res)
    wolf_quote = generate_random_wolf_quote()
    caption = ESTIMATED_PRICE.substitute(
        price=price, tags_and_confidences=tags_and_confidences, wolf_quote=wolf_quote
    )
    # Price is estimated using Sothebys.com data
    # Other characteristics can be predicted using Wikiart data

    if update.message is not None:
        await update.message.reply_photo(
            update.message.photo[-1].file_id, caption=caption
        )
        await update.message.reply_text(CONTINUE_UPLOAD_IMAGE)

    return PRICE_PREDICTOR_BY_UPLOAD_IMAGE


async def upload(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Upload image."""

    query = update.callback_query

    if query is not None:
        await query.edit_message_text(UPLOAD_FILE)

    return PRICE_PREDICTOR_BY_UPLOAD_IMAGE
