"""Bot."""

from art_price_predictor_bot.bot.generate_image import (
    continue_generate,
    estimate_generated_image,
    generate_image,
    type_text_to_generate,
)
from art_price_predictor_bot.bot.messages import (
    DEFAULT_MESSAGE,
    DONT_GET_PHOTO,
    HELP,
    STOPPED_BOT,
    UPLOAD_PHOTO_PLEASE,
)
from art_price_predictor_bot.bot.price_predict_by_upload_image import (
    estimate_price,
    upload,
)
from art_price_predictor_bot.bot.wolf_memes import give_meme, show_wolf_meme, wolf_meme
from art_price_predictor_bot.emodzi import ROCKET, WOLF
from art_price_predictor_bot.settings import (
    GENERATE_IMAGE,
    PRICE_PREDICTOR_BY_UPLOAD_IMAGE,
    START,
    TELEGRAM_TOKEN,
    WOLF_MEMES,
)
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import (
    ApplicationBuilder,
    CallbackQueryHandler,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
)

BUTTON_SIZE = 3


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Start key boars.."""
    keyboard = [
        [
            InlineKeyboardButton("Help", callback_data="help"),
            InlineKeyboardButton(
                "Estimate price" + ROCKET, callback_data="estimate_price"
            ),
        ],
        [InlineKeyboardButton("Generate Image" + WOLF, callback_data="generate")],
        [InlineKeyboardButton("Show wolf memes" + WOLF, callback_data="wolf_meme")],
    ]

    reply_markup = InlineKeyboardMarkup(keyboard)

    if update.message is not None:
        await update.message.reply_text("Choose any option", reply_markup=reply_markup)

    return START


async def help_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Help command."""
    query = update.callback_query
    if query is not None:
        await query.edit_message_text(HELP)

    return START


async def stop(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Stops estimate prices."""

    if update.message is not None:
        await update.message.reply_text(STOPPED_BOT)

    return ConversationHandler.END


async def default_message_handler(
    update: Update, context: ContextTypes.DEFAULT_TYPE
) -> None:
    """Default message handler."""
    if update.message is not None:
        await update.message.reply_text(DEFAULT_MESSAGE)


async def upload_photo_please(
    update: Update, context: ContextTypes.DEFAULT_TYPE
) -> None:
    """Upload photo please."""

    if update.message is not None:
        await update.message.reply_text(UPLOAD_PHOTO_PLEASE)


async def dont_eat_photo(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Don't eat photo."""

    if update.message is not None:
        await update.message.reply_text(DONT_GET_PHOTO)


def main():
    """Main."""
    app = ApplicationBuilder().token(TELEGRAM_TOKEN).build()
    default = MessageHandler(~filters.TEXT & ~filters.COMMAND, default_message_handler)
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            START: [
                CallbackQueryHandler(help_command, pattern="^help$"),
                CallbackQueryHandler(upload, pattern="^estimate_price$"),
                CallbackQueryHandler(type_text_to_generate, pattern="^generate$"),
                CallbackQueryHandler(wolf_meme, pattern="^wolf_meme$"),
                default,
                MessageHandler(filters.PHOTO, dont_eat_photo),
                CommandHandler("stop", stop),
            ],
            PRICE_PREDICTOR_BY_UPLOAD_IMAGE: [
                MessageHandler(filters.PHOTO, estimate_price),
                CommandHandler("stop", stop),
                default,
                MessageHandler(~filters.PHOTO, upload_photo_please),
            ],
            GENERATE_IMAGE: [
                MessageHandler(filters.TEXT & ~filters.COMMAND, generate_image),
                CallbackQueryHandler(
                    estimate_generated_image, pattern="^estimate_generated_image$"
                ),
                CallbackQueryHandler(continue_generate, pattern="^continue_generate$"),
                CommandHandler("stop", stop),
                default,
                MessageHandler(filters.PHOTO, dont_eat_photo),
            ],
            WOLF_MEMES: [
                MessageHandler(filters.TEXT & ~filters.COMMAND, give_meme),
                CallbackQueryHandler(show_wolf_meme, pattern="^wolf_meme$"),
                CommandHandler("stop", stop),
                default,
                MessageHandler(filters.PHOTO, dont_eat_photo),
            ],
        },
        fallbacks=[CommandHandler("start", start)],
    )

    app.add_handler(conv_handler)

    app.run_polling()


if __name__ == "__main__":
    main()
