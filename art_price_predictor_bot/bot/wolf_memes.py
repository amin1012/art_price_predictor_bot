"""Wolf memes."""


from pathlib import Path
from random import choice

from art_price_predictor_bot.bot.messages import STOP_WATCH_MEMES, WATCH_MEMES
from art_price_predictor_bot.settings import (
    GENERATED_IMAGE_FILE_NAME,
    WOLF_MEMES,
    WOLF_MEMES_LIST,
)
from loguru import logger
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import ContextTypes


async def wolf_meme(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Switch to wolf memes."""

    query = update.callback_query
    if query is not None and query.message is not None:
        await query.message.reply_text(WATCH_MEMES)

    return WOLF_MEMES


async def give_meme(update: Update, context: ContextTypes) -> int:
    """Give first meme."""

    photo: bytes = await choose_wolf_meme()
    if update.message is not None:
        await update.message.reply_photo(photo)

        keyboard = [[InlineKeyboardButton("Continue", callback_data="wolf_meme")]]
        reply_markup = InlineKeyboardMarkup(keyboard)
        await update.message.reply_text(STOP_WATCH_MEMES, reply_markup=reply_markup)
    return WOLF_MEMES


async def choose_wolf_meme() -> bytes:
    """Choose meme."""

    meme: str = choice(WOLF_MEMES_LIST)
    meme_path: Path = GENERATED_IMAGE_FILE_NAME.parent.parent / f"wolf_memes/{meme}"
    return meme_path.read_bytes()


async def show_wolf_meme(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Wolf meme."""
    if update.effective_user is not None:
        logger.info(f"Show wolf meme to {update.effective_user.username}")
    query = update.callback_query
    photo: bytes = await choose_wolf_meme()
    if query is not None and query.message is not None:
        await query.message.reply_photo(photo)

    keyboard = [[InlineKeyboardButton("Continue", callback_data="wolf_meme")]]

    reply_markup = InlineKeyboardMarkup(keyboard)

    if query is not None and query.message is not None:
        await query.message.reply_text(STOP_WATCH_MEMES, reply_markup=reply_markup)

    return WOLF_MEMES
