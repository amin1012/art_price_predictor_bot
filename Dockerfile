FROM python:3.9-slim

ENV WORKDIR="/app"

COPY . $WORKDIR
WORKDIR $WORKDIR

RUN pip install poetry --only main
RUN poetry install -n --no-ansi --only main

CMD ["poetry", "run", "python", "art_price_predictor_bot/bot/bot.py"]
