# Art Price Predictor Bot Release Notes


## 1.0.0
* Added buttons, generating images and price estimation for them.

## 0.2.0
* Deploy added

## 0.1.0
* Init version.
