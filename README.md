# How ro run localy?

* Go to @BotFather and receive TOKEN
* Get Secrets for FastApi art_price_predictor
* Get API_KEY, API_SECRET for Imagga:
``` https://imagga.com/```
* GET API_KEY for Journey Hugging Face model
``` https://huggingface.co/settings/tokens```
* Add it using:
```bash
$ export TELEGRAM_TOKEN=YOUR_TOKEN
$ export IMAGGA_APIKEY=...
$ export IMAGGA_SECRET=...
$ export JOURNEY_TOKEN=...
```

# To run bot locally:
* First of all get poetry env and install libs
```bash
git clone ....
cd art_price_predictor_bot
poetry env use python3.9
poetry install
```

* Now all ready to run:

```bash
poetry run python art_price_predictor_bot/bot/bot.py
```

# How to run in Docker?
```bash
docker build --tag art_price_predictor_bot ./
docker run --network="host" art_price_predictor_bot
```

